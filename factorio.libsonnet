{
  _config+:: {
    factorio: {
      name: "factorio",
      image: {
        repo: "docker.io/ofsm/ofsm",
        tag: "develop"
      },
      storageClass: "",
      storageSize: "2Gi",
    },
  },

  local k = import "ksonnet-util/kausal.libsonnet",
  local statefulset = k.apps.v1.statefulSet,
  local container = k.core.v1.container,
  local env = k.core.v1.envVar,
  local port = k.core.v1.containerPort,
  local service = k.core.v1.service,

  local withEnv(name, value) = container.withEnv(
    env.new(name=name, value=value)),

  factorio: {
    deployment: statefulset.new(
      name=$._config.factorio.name
      , replicas=1
      , containers=[
        container.new(
          "factorio"
          , $._config.factorio.image.repo + ":" + $._config.factorio.image.tag
        ) + container.withPorts([port.newUDP("game", 34197), port.newUDP("web", 80)])
        + container.withVolumeMounts(k.core.v1.volumeMount.new("data", "/opt/factorio", false))
      ]
    ) + statefulset.spec.withServiceName($.factorio.service.metadata.name)
    + statefulset.spec.withVolumeClaimTemplates([{
      metadata: {
        name: "data"
      },
      spec: {
        accessModes: ["ReadWriteOnce"],
        storageClassName: $._config.factorio.storageClass,
        resources: {
          requests: {
            storage: $._config.factorio.storageSize,
          }
        }
      }
    }]),
    service: k.util.serviceFor(self.deployment) + service.spec.withClusterIP("None"),
  }
}
